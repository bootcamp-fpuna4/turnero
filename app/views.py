from django.shortcuts import render
from .cargoForm import CargoFormulario,CargoModelForm
from django.conf import settings
from . import models

# Create your views here.
def Inicio (request):
    form = CargoModelForm(request.POST or None)
    #form = CargoFormulario(request.POST or None)
    if form.is_valid(): 
        #form_data = 'hola'
        # form_data = form.cleaned_data
        # print(form_data)

        # aux1 = (form_data.get("cargo_descripcion"))
        # # aux2 = (form_data.get("estado_servicio"))
        # obj = models.Cargo.objects.create(cargo_descripcion = aux1)
        instance= form.save(commit = False)
        print(instance)
        if not instance.cargo_estado :
            instance.cargo_estado = True
        instance.save()
    context = { 
        "Titulo":'Carga de Cargos',
        "form": form
    }
    return render(request,"inicio.html",context)

