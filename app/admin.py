from django.contrib import admin

# Register your models here.
from . import models
from .cargoForm import CargoModelForm
#admin.site.register(models.Estado,EstadoAdmin)

class EstadoAdmin(admin.ModelAdmin):
    list_display = ["estado_id","estado_descripcion"]  
    list_filter=["estado_descripcion"]
    list_editable=["estado_descripcion"]
    search_fields=  ["estado_descripcion"]


admin.site.register(models.Estado,EstadoAdmin)

class TurnoAdmin(admin.ModelAdmin):
    list_display = ["turno_id","estado_id","numero_turno","fecha_turno","hora_turno"]  
    list_filter=["estado_id"]
    list_editable=["fecha_turno","hora_turno"]
    search_fields=  ["fecha_turno"]


admin.site.register(models.Turno,TurnoAdmin)

class PrioridadAdmin(admin.ModelAdmin):
    list_display = ["prioridad_id","prioridad_descripcion","nivel"]  
    list_filter=["prioridad_descripcion"]
    list_editable=["prioridad_descripcion"]
    search_fields=  ["prioridad_descripcion"]

admin.site.register(models.Prioridad,PrioridadAdmin)

class SexoAdmin(admin.ModelAdmin):
    list_display = ["sexo_id","sexo_descripcion"]  
    list_filter=["sexo_descripcion"]
    list_editable=["sexo_descripcion"]
    search_fields=  ["sexo_descripcion"]


admin.site.register(models.Sexo,SexoAdmin)

class TipopersonaAdmin(admin.ModelAdmin):
    list_display = ["tipo_persona_id","tipo_persona_descripcion"]  
    list_filter=["tipo_persona_descripcion"]
    list_editable=["tipo_persona_descripcion"]
    search_fields=  ["tipo_persona_descripcion"]


admin.site.register(models.Tipopersona,TipopersonaAdmin)

class PersonaAdmin(admin.ModelAdmin):
    list_display = ["persona_id","tipo_persona_id","sexo_id","nro_documento","ruc","nombre","apellido","nro_telefono"]  
    list_filter=["apellido","nombre"]
    list_editable=["apellido","nombre"]
    search_fields=  ["nro_documento"]


admin.site.register(models.Persona,PersonaAdmin)




class PermisosAdmin(admin.ModelAdmin):
    list_display = ["permiso_id","permiso_descripcion"]  
    list_filter=["permiso_descripcion"]
    list_editable=["permiso_descripcion"]
    search_fields=  ["permiso_descripcion"]

admin.site.register(models.Permisos,PermisosAdmin)

#admin.site.register(models.Rol,RolAdmin)
#admin.site.register(models.Rolpermiso,RolpermisoAdmin)
#admin.site.register(models.Empresa,EmpresaAdmin)
#admin.site.register(models.Funcionario,FuncionarioAdmin)
class CargoAdmin(admin.ModelAdmin):
    list_display = ["cargo_id","cargo_descripcion","estado_cargo"]  
    list_filter=["cargo_descripcion"]
    list_editable=["cargo_descripcion"]
    search_fields=  ["cargo_descripcion"]
    form = CargoModelForm


admin.site.register(models.Cargo,CargoAdmin)
#admin.site.register(models.Usuario,UsuarioAdmin)
#admin.site.register(models.Cargofuncionario,CargofuncionarioAdmin)
#admin.site.register(models.Permisos,PermisosAdmin)
#admin.site.register(models.Puesto,PuestoAdmin)
#admin.site.register(models.Funcionariopuesto,FuncionarioPuestoAdmin)
#admin.site.register(models.Cliente,ClienteAdmin)
#admin.site.register(models.Cola,ColaAdmin)