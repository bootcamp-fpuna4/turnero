from django import forms
from .models import Cargo

class CargoModelForm(forms.ModelForm):
    class Meta:
        model = Cargo
        fields = ["cargo_descripcion"]
    

class CargoFormulario(forms.Form):
    cargo_descripcion = forms.CharField(max_length=20,required=True)