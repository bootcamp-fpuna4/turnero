# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models

class Estado(models.Model):
    estado_id = models.IntegerField(primary_key=True)
    estado_descripcion = models.CharField(max_length=20)

    class Meta:
      #  managed = False
        db_table = 'estado'

class Turno(models.Model):
    turno_id = models.AutoField(primary_key=True)
    estado = models.ForeignKey(Estado, on_delete=models.CASCADE)
    numero_turno = models.CharField(max_length=3)
    fecha_turno = models.DateField()
    hora_turno = models.TimeField()

    class Meta:
     #   managed = False
        db_table = 'turno'

class Prioridad(models.Model):
    prioridad_id = models.AutoField(primary_key=True)
    prioridad_descripcion = models.CharField(max_length=20)
    nivel = models.CharField(max_length=20)
    usuario_modificacion = models.CharField(max_length=16)
    usuario_insercion = models.CharField(max_length=16)
    fecha_insercion = models.DateTimeField()
    fecha_modificacion = models.DateTimeField()

    class Meta:
     #   managed = False
        db_table = 'prioridad'

class Sexo(models.Model):
    sexo_id = models.AutoField(primary_key=True)
    sexo_descripcion = models.CharField(max_length=20)
    estado_sexo = models.BooleanField()
    fecha_insercion = models.DateTimeField()
    usuario_insercion = models.CharField(max_length=16)
    fecha_modificacion = models.DateTimeField()
    usuario_modificacion = models.CharField(max_length=16)

    class Meta:
       # managed = False
        db_table = 'sexo'

class Tipopersona(models.Model):
    tipo_persona_id = models.AutoField(primary_key=True)
    tipo_persona_descripcion = models.CharField(max_length=20)
    fecha_insercion = models.DateTimeField()
    usuario_modificacion = models.CharField(max_length=16)
    fecha_modificacion = models.DateTimeField()
    usuario_insercion = models.CharField(max_length=16)

    class Meta:
     #   managed = False
        db_table = 'tipopersona'


class Persona(models.Model):
    persona_id = models.AutoField(primary_key=True)
    tipo_persona = models.ForeignKey(Tipopersona, on_delete=models.CASCADE)
    sexo = models.ForeignKey(Sexo, on_delete=models.CASCADE)
    nro_documento = models.CharField(max_length=20, blank=True, null=True)
    ruc = models.CharField(max_length=15, blank=True, null=True)
    nombre = models.CharField(max_length=100)
    nro_telefono = models.CharField(max_length=15)
    apellido = models.CharField(max_length=100)
    direccion = models.CharField(max_length=100, blank=True, null=True)
    fecha_nacimiento = models.DateField(blank=True, null=True)
    fecha_insercion = models.DateTimeField()
    usuario_modificacion = models.CharField(max_length=16)
    fecha_modificacion = models.DateTimeField()
    usuario_insercion = models.CharField(max_length=16)
    estado_persona = models.BooleanField()

    class Meta:
      #  managed = False
        db_table = 'persona'

class Permisos(models.Model):
    permiso_id = models.AutoField(primary_key=True)
    permiso_descripcion = models.CharField(max_length=50)
    usuario_insercion = models.CharField(max_length=16)
    fecha_insercion = models.DateTimeField()
    usuario_modificacion = models.CharField(max_length=16)
    fecha_modificacion = models.DateTimeField()
    estado_permiso = models.BooleanField()

    class Meta:
     #   managed = False
        db_table = 'permisos'



class Rol(models.Model):
    rol_id = models.AutoField(primary_key=True)
    fecha_insercion = models.DateTimeField()
    usuario_modificacion = models.CharField(max_length=16)
    fecha_modificacion = models.DateTimeField()
    usuario_insercion = models.CharField(max_length=16)
    rol_descripcion = models.CharField(max_length=20)
    estado_rol = models.BooleanField()

    class Meta:
      #  managed = False
        db_table = 'rol'

class Rolpermiso(models.Model):
    rol = models.OneToOneField(Rol, on_delete=models.CASCADE, primary_key=True)
    permiso = models.ForeignKey(Permisos, on_delete=models.CASCADE)

    class Meta:
     #   managed = False
        db_table = 'rolpermiso'
        unique_together = (('rol', 'permiso'),)

class Empresa(models.Model):
    empresa_id = models.IntegerField(primary_key=True)
    nro_telefono = models.CharField(max_length=15)
    fecha_constitucion = models.DateField(blank=True, null=True)
    direccion = models.CharField(max_length=100, blank=True, null=True)
    nombre = models.CharField(max_length=100)

    class Meta:
    #    managed = False
        db_table = 'empresa'




class Funcionario(models.Model):
    funcionario_id = models.IntegerField(primary_key=True)
    persona = models.ForeignKey(Persona, on_delete=models.CASCADE)
    empresa = models.ForeignKey(Empresa, on_delete=models.CASCADE)
    fecha_inicio = models.DateField()
    fecha_fin = models.DateField(blank=True, null=True)
    estado_funcionario = models.BooleanField()
    fecha_insercion = models.DateTimeField()
    usuario_modificacion = models.CharField(max_length=16)
    fecha_modificacion = models.DateTimeField()
    usuario_insercion = models.CharField(max_length=16)

    class Meta:
      #  managed = False
        db_table = 'funcionario'

class Cargo(models.Model):
    cargo_id = models.AutoField(primary_key=True)
    estado_cargo = models.BooleanField(null=True)
    cargo_descripcion = models.CharField(max_length=20)
    fecha_insercion = models.DateTimeField(null=True)
    usuario_modificacion = models.CharField(max_length=16,null=True)
    fecha_modificacion = models.DateTimeField(null=True)
    usuario_insercion = models.CharField(max_length=16,null=True)

    def __str__(self) -> str:
        return self.cargo_descripcion
    class Meta:
       # managed = False
        db_table = 'cargo'

class Usuario(models.Model):
    usuario_id = models.AutoField(primary_key=True)
    rol = models.ForeignKey(Rol, on_delete=models.CASCADE)
    funcionario = models.ForeignKey(Funcionario, on_delete=models.CASCADE)
    nombre_usuario = models.CharField(max_length=20)
    contrasenha_usuario = models.CharField(max_length=10)
    estado_usuario = models.BooleanField()
    fecha_insercion = models.DateTimeField()
    usuario_modificacion = models.CharField(max_length=16)
    fecha_modificacion = models.DateTimeField()
    usuario_insercion = models.CharField(max_length=16)

    class Meta:
      #  managed = False
        db_table = 'usuario'


class Cargofuncionario(models.Model):
    cargo = models.OneToOneField(Cargo, on_delete=models.CASCADE, primary_key=True)
    funcionario = models.ForeignKey(Funcionario, on_delete=models.CASCADE)
    fecha_inicio_cargo = models.DateField()
    fecha_fin_cargo = models.DateField()

    class Meta:
      #  managed = False
        db_table = 'cargofuncionario'
        unique_together = (('cargo', 'funcionario'),)

class Servicios(models.Model):
    servicio_id = models.IntegerField(primary_key=True)
    servicio_descripcion = models.CharField(max_length=20)
    estado_servicio = models.BooleanField()
    fecha_insercion = models.DateTimeField()
    usuario_modificacion = models.CharField(max_length=16)
    fecha_modificacion = models.DateTimeField()
    usuario_insercion = models.CharField(max_length=16)

    class Meta:
      #  managed = False
        db_table = 'servicios'

class Puesto(models.Model):
    puesto_id = models.IntegerField(primary_key=True)
    servicio = models.ForeignKey(Servicios, on_delete=models.CASCADE)
    puesto_descripcion = models.CharField(max_length=20)
    fecha_insercion = models.DateTimeField()
    usuario_modificacion = models.CharField(max_length=16)
    fecha_modificacion = models.DateTimeField()
    usuario_insercion = models.CharField(max_length=16)

    class Meta:
       # managed = False
        db_table = 'puesto'

class Funcionariopuesto(models.Model):
    funcionario = models.OneToOneField(Funcionario, on_delete=models.CASCADE, primary_key=True)
    puesto = models.ForeignKey(Puesto, on_delete=models.CASCADE)
    fecha_puesto = models.DateTimeField()

    class Meta:
     #   managed = False
        db_table = 'funcionariopuesto'
        unique_together = (('funcionario', 'puesto'),)



class Cliente(models.Model): 
    cliente_id = models.AutoField(primary_key=True)
    persona = models.ForeignKey(Persona, on_delete=models.CASCADE)
    fecha_insercion = models.DateTimeField()
    usuario_modificacion = models.CharField(max_length=16)
    fecha_modificacion = models.DateTimeField()
    usuario_insercion = models.CharField(max_length=16)
    estado_cliente = models.BooleanField()

    class Meta:
    #    managed = False
        db_table = 'cliente'


class Cola(models.Model):
    cola_id = models.AutoField(primary_key=True)
    cliente = models.ForeignKey(Cliente, on_delete=models.CASCADE)
    servicio = models.ForeignKey(Servicios, on_delete=models.CASCADE)
    prioridad = models.ForeignKey(Prioridad, on_delete=models.CASCADE)
    turno_anterior = models.CharField(max_length=10)
    turno = models.ForeignKey(Turno, on_delete=models.CASCADE)
    turno_siguiente = models.CharField(max_length=10)
    fecha_insercion = models.DateTimeField()
    fecha_modificacion = models.DateTimeField()
    usuario_insercion = models.CharField(max_length=16)
    usuario_modificacion = models.CharField(max_length=16)

    class Meta:
     #   managed = False
        db_table = 'cola'



class DjangoMigrations(models.Model):
    id = models.BigAutoField(primary_key=True)
    app = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    applied = models.DateTimeField()

    class Meta:
      #  managed = False
        db_table = 'django_migrations'
















